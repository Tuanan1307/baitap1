let Product = [
  { name: "CPU", price: 750, quality: 10, categoryId: 1 },
  { name: "RAM", price: 50, quality: 2, categoryId: 2 },
  { name: "HDD", price: 70, quality: 1, categoryId: 2 },
  { name: "Main", price: 400, quality: 3, categoryId: 1 },
  { name: "Keyboard", price: 30, quality: 8, categoryId: 4 },
  { name: "Mouse", price: 25, quality: 50, categoryId: 4 },
  { name: "VGA", price: 60, quality: 35, categoryId: 3 },
  { name: "Monitor", price: 120, quality: 28, categoryId: 2 },
  { name: "Case", price: 120, quality: 28, categoryId: 5 },
];
let Category = [
  { id: 1, name: "Comuter" },
  { id: 2, name: "Memory" },
  { id: 3, name: "Card" },
  { id: 4, name: "Acsesory" },
];

// function sortCategory(listProduct, listCategory) {

//   return product;
// }
function sortByCategoryName(listProduct, listCategory) {
  for (let i = 1; i < listCategory.length; i++) {
    let key = listCategory[i];
    let j = i - 1;

    while (j >= 0 && key.name.localeCompare(listCategory[j].name) <= 0) {
      listCategory[j + 1] = listCategory[j];
      --j;
    }
    listCategory[j + 1] = key;
  }

  let product = [];
  for (let i = 0; i < listCategory?.length; i++) {
    for (let j = -1; j < listProduct?.length; j++) {
      if (listCategory[i]?.id == listProduct[j]?.categoryId) {
        product.push(listProduct[j]);
      }
    }
  }
  return product;
}
let abc = sortByCategoryName(Product, Category);

console.log(abc);
