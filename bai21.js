// de quy
function calSalaryDeQuy(salary, n) {
  if (n == 1) {
    return salary;
  }
  return calSalaryDeQuy(salary, n - 1) * 0.1 + calSalaryDeQuy(salary, n - 1);
}

calSalaryDeQuy(10, 4);
// khong de quy
function calSalary(salary, n) {
  for (let i = 1; i < n; i++) {
    salary = salary + salary * 0.1;
  }
  return salary;
}
calSalary(10, 4);
